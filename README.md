# UiTDatabank to DOCX script

## Installatie
Om dit script te kunnen gebruiken, heb je enkele courante Python libraries nodig.
Zie hiervoor het bestand requirements.txt.

Gebruik bij voorkeur een virtuele omgeving zoals venv.
Om de vereiste bibliotheken te installeren, voer je vanop de terminal de instructie `pip install -r requirements.txt` uit.

## Aan de slag
De code van het bestand is zo goed mogelijk gedocumenteerd. Mits enige basiskennis Python (3) zou het mogelijk moeten zijn dit bestand aan te passen aan de eigen wensen.

* De functie `create_word_document` bepaalt welke velden ingeladen moeten worden
* De functie `load_list` haalt alle benodigde velden op uit de spreadsheet eventueel aangevuld met de online gegevens
* De functie `create_indented_paragraph` bepaalt hoe itemvelden er zullen uitzien (lege velden worden overgeslagen)
* De functie `get_event` haalt eventuele bijkomende informatie op van het internet

De standaardnaam voor de spreadsheet is `udb.xls` en het standaard werkblad heet `Worksheet`.
Het gegenereerde document heet standaard `kalender.docx` en de afbeeldingen worden standaard opgeslagen in een map `images/`

