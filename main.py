#!/usr/bin/python3

from docx import Document
from docx.shared import Cm
import json
import requests
from openpyxl import load_workbook
from io import StringIO, BytesIO
from html.parser import HTMLParser
from os import makedirs

#stukken code geleend van:
# https://stackoverflow.com/questions/753052/strip-html-from-strings-in-python (strip HTML)

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.text = StringIO()
    def handle_data(self, d):
        self.text.write(d)
    def get_data(self):
        return self.text.getvalue()

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

# Een lege klasse waarin dan de relevante velden toegevoegd kunnen worden
class Activiteit: pass

# Haal informatie over het event op van de UiTDatabank
# Dit kan nuttig zijn om specifieke velden te gebruiken die niet aanwezig zijn in de export
# In dit voorbeeld wordt de begindatum ingeladen - waarop dan gesorteerd zal worden.
# Andere toepassingen zouden kunnen zijn: meertalige info, availability, ...
def get_event(cdbid, fields={'startDate'}):
    dictFields = {} # een lege lijst om mee te starten
    url = requests.get("https://io.uitdatabank.be/event/" + cdbid)
    raw = url.text
    data = json.loads(raw)

    # We hebben niet alle info nodig, maar alleen de gemarkeerde velden.
    # We voorzien een uitweg in geval een veld niet gevonden wordt (of er een andere foutmelding is)
    for field in fields:
        try:
            dictFields[field] = data[field]
        except Exception as e:
            dictFields[field] = "N/B"
            print(f"### ERROR: {e}")
    return dictFields

# Open de spreadsheet, standaard met de naam udb.xlsx, met de gegevens in het werkblad "Worksheet"
def load_list(workbook='udb.xlsx', worksheet='Worksheet'):
    eventsArray = []
    wb = load_workbook(workbook)
    ws = wb[worksheet]
    for r in range(2, ws.max_row+1):
        event = Activiteit()
        event.cdbid = ws.cell(row=r, column=1).value         # CDBID is een essentieel veld. In onze spreadsheet staat dit in kolom 1
        if event.cdbid == "":
            # lege lijnen worden overgeslagen
            break
        event.titel = ws.cell(row=r, column=2).value        # de titel staat in kolom 2 van onze spreadsheet
        event.datum = ws.cell(row=r, column=4).value        # De datumbeschrijving staat in kolom 4
        event.contact_email = ws.cell(row=r, column=8).value
        event.contact_tel = ws.cell(row=r, column=9).value
        event.contact_url = ws.cell(row=r, column=10).value

        # get_event kan meerdere velden ineens ophalen uit UiTDatabank. Om het niet te traag te maken,
        # doen we maar één call, en in fields bepalen we welke velden we nodig hebben
        # op dit ogenblik kan het maar één niveau diep gaan
        # als een veld niet gevonden wordt, krijgt het de waarde "N/B"
        fields = get_event(cdbid=event.cdbid, fields={'startDate', 'image', 'creator', 'description'})

        # velden van de activiteit worden opgehaald uit de vorige instructie
        event.startDate = fields['startDate']
        event.imageURL = fields['image']
        event.creator = fields['creator']

        # Als iets uit een dieper niveau gehaald moet worden, moeten we opvangen dat dit veld ook "N/B" kan zijn.
        try:
            event.description = strip_tags(fields['description']['nl'])
        except Exception as e:
            # Als dit niet gelukt is, geven we dit veld de waarde "N/B" (ongeacht welke foutmelding)
            event.description = "N/B"
            print(f"### ERROR: {e}")
        print(f">> Gegevens ophalen voor evenement: {event.titel}")
        eventsArray.append(event) # Deze activiteit wordt aan het lijstje klaargemaakte activiteiten toegevoegd
    return eventsArray

# Stel de opmaak in die gebruikt zal worden voor ingesprongen alinea's. Hier is dit bewust simpel gehouden.
def create_indented_paragraph(document, title="", field=""):
    try:
        if len(field) > 0:
            p = document.add_paragraph()                # De alinea wordt aangemaakt
            formatting = p.paragraph_format             # De alinea-opmaak wordt klaargezet
            formatting.left_indent = Cm(1.5)            # De alinea spring 1,5cm links in...
            formatting.first_line_indent = Cm(-1.5)     # ... om daarna de 1e regel 1,5cm omgekeerd te laten inspringen
            p.add_run({title}).bold = True              # Dit stukje tekst moet vet komen
            p.add_run(f"\t{field}")                     # Dit stukje tekst is gewone tekst
    except TypeError:
        # If field is an invalid type, just ignore this field (usually NoneType)
        pass

## Genereert een Word document op basis van de doorgegeven array van objecten
def create_word_document(filename="kalender.docx", events={}, title="Evenementenkalender"):
    document = Document()               # Maak het basisdocument in Python
    document.add_heading(title, 0)      # Titel voor het volledige document
    makedirs('images', exist_ok=True)   # Maak een "images" map als die nog niet bestaat
    for event in events:
        print(f">> Toevoegen aan document: {event.titel}")
        document.add_heading(f"{event.titel}", 2)   # Titel per activiteit (Kop 2)

        # De afbeelding wordt opgehaald en in het document geplaatst
        try:
            img_data = requests.get(event.imageURL).content
            with open(f"images/{event.cdbid}.jpg", 'wb+') as handler:
                handler.write(img_data)
            document.add_picture(f"images/{event.cdbid}.jpg", width=Cm(5))
            create_indented_paragraph(document=document, title="Lokale afbeelding:", field=f"images/{event.cdbid}.jpg")
        except Exception as e:
            print(f"### ERROR: {e}")

        document.add_paragraph(event.description)  # Beschrijving wordt toegevoegd

        # Velden die ingeladen werden, worden hier netjes in een alinea gezet volgens de
        # specificaties verwerkt in de functie create_indented_paragraph
        create_indented_paragraph(document=document, title="Wanneer:", field=event.datum)
        create_indented_paragraph(document=document, title="Contact e-mail:", field=event.contact_email)
        create_indented_paragraph(document=document, title="Contact telefoon:", field=event.contact_tel)
        create_indented_paragraph(document=document, title="Website:", field=event.contact_url)
        create_indented_paragraph(document=document, title="URL Afbeelding:", field=event.imageURL)
        document.add_page_break()  # Een apart pagina-einde na elke document

    print(f">> Document {filename} wordt opgeslagen")
    document.save(filename)


if __name__ == '__main__':
    # Spreadsheet inladen en verrijken met de nodige velden uit UDB
    # Bekijk de functie "load_list" om die aan te passen
    events = load_list(workbook="udb.xlsx")

    # Sorteer de gegenereerde lijst op datum
    events.sort(key=lambda x: x.startDate, reverse=False)

    # Maak het Word-document met de bestandsnaam.
    create_word_document(filename="kalender.docx", events=events)
